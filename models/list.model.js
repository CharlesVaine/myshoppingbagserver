const mongoose = require("mongoose");

const ListSchema = new mongoose.Schema({
  title: { type: String, required: true, unique: true },
  items: { type: Array, default: [] }
});

module.exports = new mongoose.model("List", ListSchema);
