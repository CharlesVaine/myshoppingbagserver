var express = require("express");
var router = express.Router();
const ListModel = require("../models/list.model");

router
  .route("/")
  .get((req, res) => {
    ListModel.find()
      .then(lists => res.status(200).send(lists.map(list => mapList(list))))
      .catch(() => res.status(500).send());
  })
  .post((req, res) => {
    const newList = new ListModel(req.body);
    newList
      .save()
      .then(list => res.status(201).send(mapList(list)))
      .catch(err => {
        if (err.code === 11000) {
          return res.status(400).send();
        }
        res.status(500).send();
      });
  });

router
  .route("/:listId")
  .get((req, res) => {
    ListModel.findOne({ _id: req.params.listId })
      .then(list => {
        if (!list) {
          return res.status(404).send();
        }
        res.status(200).send(mapList(list));
      })
      .catch(() => res.status(500).send());
  })
  .put((req, res) => {
    ListModel.findOneAndUpdate({ _id: req.params.listId }, req.body, {
      new: true
    })
      .then(list => {
        if (!list) {
          return res.status(404).send();
        }
        res.status(200).send(mapList(list));
      })
      .catch(() => res.status(500).send());
  })
  .delete((req, res) => {
    ListModel.deleteOne({ _id: req.params.listId })
      .then(deleted => {
        if (!deleted.n) {
          return res.status(404).send();
        }
        res.status(200).send();
      })
      .catch(() => res.status(500).send());
  });

function mapList(list) {
  const { _id, title, items } = list;
  return { _id, title, items };
}

module.exports = router;
