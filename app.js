var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const mongoose = require("mongoose");
const db_path = process.env.DB_PATH || "localhost:27017";
const db_name = process.env.DB_NAME || "shoppingbag";

var listRouter = require("./routes/list");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/list", listRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

mongoose
  .connect(`mongodb://${db_path}/${db_name}`, { useNewUrlParser: true })
  .then(() => console.info(`Success connecting to "${db_name}" Database`))
  .catch(() => console.error(`Error connecting to "${db_name}" Database`));

module.exports = app;
